function layThongTinTuForm(){
    var maSv = document.getElementById('txtMaSV').value;
    var tenSv = document.getElementById('txtTenSV').value;
    var email = document.getElementById('txtEmail').value;
    var matKhau = document.getElementById('txtPass').value;
    var diemToan = document.getElementById('txtDiemToan').value;
    var diemLy = document.getElementById('txtDiemLy').value;
    var diemHoa = document.getElementById('txtDiemHoa').value;
   
    var sv = new sinhVien(maSv, tenSv,email, matKhau,diemToan,diemLy,diemHoa)
    // sv de nhu vay  khong on nen se tach thanh model rieng de de quan ly
    return sv
}

function renderDssv(list){
    var contentHTML = ""
    for(var i =0; i<list.length;i++){
        var currentSv = list[i]
        var contentTr = `<tr>
        <td>${currentSv.ma}</td>
        <td>${currentSv.ten}</td>
        <td>${currentSv.email}</td>
        <td>${currentSv.tinhDTB()}</td>
        <td>
        <button onclick="xoaSv('${currentSv.ma}')" class="btn btn-danger">Xoa</button>
        </td>
        <td>
        <button onclick="suaSv('${currentSv.ma}')" class="btn btn-warning">Sua</button>
        </td>
        </tr>`
        contentHTML += contentTr
    }
document.getElementById('tbodySinhVien').innerHTML= contentHTML
}

function saveLocalStorage(){
    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV",dssvJson);
}

function showThongTinSinhVien(sinhVien){
document.getElementById('txtMaSV').value = sinhVien.ma;
document.getElementById('txtTenSV').value = sinhVien.ten;
document.getElementById('txtEmail').value = sinhVien.email;
document.getElementById('txtPass').value = sinhVien.matKhau;
document.getElementById('txtDiemToan').value = sinhVien.diemToan;
document.getElementById('txtDiemLy').value = sinhVien.diemLy;
document.getElementById('txtDiemHoa').value = sinhVien.diemHoa
;
}
function reset(){
    document.getElementById('formQLSV').reset()
}