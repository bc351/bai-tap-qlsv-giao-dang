var dssv = [];
var dataJson = localStorage.getItem("DSSV")
var result = [];
if(dataJson){
    var dataRaw = JSON.parse(dataJson);
    
    for(var index = 0; index<dataRaw.length; index++){
        currentData = dataRaw[index];
        // khoi tao lai newSv
        var newSv = new sinhVien(currentData.ma, currentData.ten, currentData.email, currentData.matKhau, currentData.diemToan, currentData.diemLy, currentData.diemHoa)
        result.push(newSv)
    }

    dssv = result
    renderDssv(dssv)
}

function themSv(){
    // lay du lieu tu localStorage len
 
  
   var newSv = layThongTinTuForm()
    dssv.push(newSv)
    console.log('dssv: ', dssv);
    // show thong tin len form
    renderDssv(dssv)
// tach ham sang file svController
// save xuong localStorage
saveLocalStorage()
// sau khi save localStorage thi mat di function do JSON

reset()
}
function xoaSv(idSv){
    console.log('idSv: ', idSv);
    // chuyen tu idSv thanh index
    var index = dssv.findIndex(function(sv){
       return sv.ma == idSv
    })
    if(index == -1) return
    console.log('index: ', index);
    // array.splice(vi tri, so luong)
    dssv.splice(index,1)
    console.log('dssv:', dssv);
    renderDssv(dssv)
    saveLocalStorage()
 
}
function suaSv(idSv){
  
    var index = dssv.findIndex(function(sv){
        return sv.ma == idSv
     })
     if(index == -1) return
     
     var sv = dssv[index]
     showThongTinSinhVien(sv)
     document.getElementById('txtMaSV').disabled = true;
}

function capNhat(){
   var svEdit = layThongTinTuForm()
   var index = dssv.findIndex(function(sv){
    return sv.ma == svEdit.ma
 })
 if(index == -1) return
 console.log('index: ', index);
   dssv[index]= svEdit
   renderDssv(dssv)
   saveLocalStorage()
   document.getElementById('txtMaSV').disabled = false
   reset()

//    lien quan den index thi phai tim index truoc
}

function search(){
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById('txtSearch');
    filter = input.value.toUpperCase();
    table = document.getElementById('tbodySinhVien');
    tr = table.getElementsByTagName('tr');
    for(i=0; i<tr.length; i++){
        td = tr[i].getElementsByTagName('td')[1];
        if(td){
            txtValue = td.textContent || td.innerText;
            if(txtValue.toUpperCase().indexOf(filter)>-1){
                tr[i].style.display = ""
            }else{
                tr[i].style.display = "none"
            }
        }
    }
}